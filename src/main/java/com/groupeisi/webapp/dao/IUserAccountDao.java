package com.groupeisi.webapp.dao;

import com.groupeisi.webapp.entity.UserAccountEntity;

import java.util.Optional;

public interface IUserAccountDao {


    Optional<UserAccountEntity> login(String email, String password);
}
