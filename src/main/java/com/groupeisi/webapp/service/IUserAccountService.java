package com.groupeisi.webapp.service;

import com.groupeisi.webapp.dto.UserAccountDto;

import java.util.Optional;

public interface IUserAccountService {

    Optional<UserAccountDto> login(String email, String password);
}
