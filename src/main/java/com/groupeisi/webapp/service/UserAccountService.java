package com.groupeisi.webapp.service;

import com.groupeisi.webapp.dao.IUserAccountDao;
import com.groupeisi.webapp.dao.UserAccountDao;
import com.groupeisi.webapp.dto.UserAccountDto;
import com.groupeisi.webapp.entity.UserAccountEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Optional;

public class UserAccountService implements IUserAccountService {
    private IUserAccountDao userAccountDao = new UserAccountDao();
    private Logger logger = LoggerFactory.getLogger(UserAccountService.class);

    @Override
    public Optional<UserAccountDto> login(String email, String password) {
        Optional<UserAccountEntity> userAccountEntity = userAccountDao.login(email, password);
        if(userAccountEntity.isPresent()) {
            logger.info("User account logged in with email: {}" , email);
            return Optional.of(new UserAccountDto());
        }
        logger.error("User account not logged in with email: {}" , email);
        return Optional.empty();
    }

    public void setUserAccountDao(IUserAccountDao userAccountDao) {
        this.userAccountDao = userAccountDao;
    }
}
