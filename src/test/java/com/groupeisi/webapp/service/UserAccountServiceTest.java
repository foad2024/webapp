package com.groupeisi.webapp.service;

import com.groupeisi.webapp.dao.UserAccountDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserAccountServiceTest {
    @InjectMocks
    private UserAccountService userAccountService;
    private UserAccountDao userAccountDao;

    @BeforeEach
    void Init(){
        userAccountDao = mock(UserAccountDao.class);
        userAccountService.setUserAccountDao(userAccountDao);
    }
    @Test
    void loginOK(){
        Assertions.assertTrue(true);
    }
    @Test
    void loginFail(){
        Assertions.assertTrue(true);
    }

}